package com.vibe.britevibe.View

import android.app.Instrumentation
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import org.junit.After
import org.junit.Before
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

class SplashScreenTest {

    @get:Rule // unresolved reference here
    val activityRule = ActivityTestRule(SplashScreen::class.java)
    var mActivity:SplashScreen ?= null
    var monitor:Instrumentation.ActivityMonitor = InstrumentationRegistry.getInstrumentation().addMonitor(HomeScreen::class.java.name,null,false)


    @Before
    fun setUp()
    {
        mActivity = activityRule.activity
    }

    @Test
    fun launchtohomescreen()
    {
       var secondActivity= InstrumentationRegistry.getInstrumentation().waitForMonitorWithTimeout(monitor,200)
       assertNotNull(secondActivity)
       secondActivity.finish()
    }

    @After
    fun tearDown()
    {
        mActivity = null
    }
}