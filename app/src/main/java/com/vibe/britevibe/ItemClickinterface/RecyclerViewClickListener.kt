package com.vibe.britevibe.ItemClickinterface

import android.view.View
import com.vibe.britevibe.model.Item


interface RecyclerViewClickListener {
    fun onRecyclerViewItemClick(view: View, movieItem: Item)
}