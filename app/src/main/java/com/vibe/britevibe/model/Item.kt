package com.vibe.britevibe.model


import org.parceler.Parcel
import org.parceler.ParcelConstructor
import org.parceler.ParcelProperty


@Parcel
data class Item @ParcelConstructor
constructor(@ParcelProperty("categories") var categories: String,
            @ParcelProperty("link") var link: String,
            @ParcelProperty("pubDate") var pubDate: String,
            @ParcelProperty("read") var read: String,
            @ParcelProperty("des") var des: String,
            @ParcelProperty("title") var title: String
){

    constructor(): this("","","", "","","")


}