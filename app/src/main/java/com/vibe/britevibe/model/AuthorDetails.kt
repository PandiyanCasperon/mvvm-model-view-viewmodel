package com.vibe.britevibe.model

import org.parceler.Parcel
import org.parceler.ParcelConstructor
import org.parceler.ParcelProperty


@Parcel
data class AuthorDetails @ParcelConstructor
constructor(@ParcelProperty("author") var author: String,
            @ParcelProperty("image") var image: String,
            @ParcelProperty("description") var description: String,
            @ParcelProperty("role") var role: String,
            @ParcelProperty("story") var story: String,
            @ParcelProperty("allcount") var allcount: String,
            @ParcelProperty("javacount") var javacount: String,
            @ParcelProperty("androidcount") var androidcount: String,
            @ParcelProperty("datastructurecount") var datastructurecount: String,
            @ParcelProperty("url") var url: String){

    constructor(): this("","","", "","","","","","","")


}