package com.vibe.britevibe.model

data class MediumArticle(
    val items: List<Item>,
    val status: String
)