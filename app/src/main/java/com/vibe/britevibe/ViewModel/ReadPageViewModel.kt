package com.vibe.britevibe.ViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.io.IOException

class ReadPageViewModel : ViewModel()
{

    private val senddocument = MutableStateFlow<Event>(Event.Empty)
    var conversion: StateFlow<Event> = senddocument

    sealed class Event
    {
        class Success(val result:Document?): Event()
        object Empty : Event()
    }

    fun execute(weburl: String) = viewModelScope.launch {
        val result = doInBackground(weburl) // runs in background thread without blocking the Main Thread
        onPostExecute(result)
    }

    private suspend fun doInBackground(weburl: String): Document? = withContext(Dispatchers.IO) { // to run code in Background Thread
        // do async work
        var document: Document? = null
        try {
            document = Jsoup.connect(weburl).get()
            document.getElementsByClass("ag ah s ai aj")
                .remove() // add the class names which you want to remove from WebView.
            document.getElementsByClass("s gh").remove()
            document.getElementsByClass("hs fi s qv").remove()
            document.getElementsByClass("fd fe sg s").remove()
            document.getElementsByClass("fd fe sf s ga fr gc fs sg sh si sj sk sl").remove()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return@withContext document
    }
    // Runs on the Main(UI) Thread
    private fun onPostExecute(document: Document?) {
        // hide progress
        senddocument.value = Event.Success(document)
    }

}