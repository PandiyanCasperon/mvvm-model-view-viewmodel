package com.vibe.britevibe.ViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.IOException

class HomePageViewModel: ViewModel() {

    lateinit var  appUpdateManager: AppUpdateManager


    fun execute() = viewModelScope.launch {
        val result = doInBackground() // runs in background thread without blocking the Main Thread
        onPostExecute()
    }


    private suspend fun doInBackground() = withContext(Dispatchers.IO) { // to run code in Background Thread
        try
        {
            getfirebasetoken()
        }
        catch (e: IOException)
        {
            e.printStackTrace()
        }
    }
    // Runs on the Main(UI) Thread
    private fun onPostExecute() {
        // Complete call of background
    }
    fun getfirebasetoken()
    {
        FirebaseMessaging.getInstance().token.addOnCompleteListener {
            if(it.isComplete){
                Timber.d("Token--->"+it.result.toString())
            }
        }
    }

}