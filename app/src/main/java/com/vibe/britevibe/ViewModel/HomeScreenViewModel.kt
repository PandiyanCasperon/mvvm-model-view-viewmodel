package com.vibe.britevibe.ViewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vibe.britevibe.Repository.ApiRepository
import com.vibe.britevibe.model.Item
import com.plcoding.currencyconverter.util.DispatcherProvider
import com.plcoding.currencyconverter.util.Resource
import com.vibe.britevibe.model.AuthorDetails
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.io.IOException

class HomeScreenViewModel @ViewModelInject constructor(
    private val repository: ApiRepository,
    private val dispatchers: DispatcherProvider
): ViewModel() {

    sealed class Event
    {
        class Success(val result: List<Item>,val author: AuthorDetails): Event()
        class Failure(val errorText: String) : Event()
        class Filtereddata(val tempdata: List<Item>) : Event()
        object Loading : Event()
        object Empty : Event()
    }

    private val _conversion = MutableStateFlow<Event>(Event.Empty)
    val conversion: StateFlow<Event> = _conversion
    var overalllist:ArrayList<Item> = arrayListOf<Item>()

    fun recentuserlist(page:Int)
    {
        viewModelScope.launch(dispatchers.io) {
            _conversion.value = Event.Loading
            when(val ratesResponse = repository.getRecentlist())
            {
                is Resource.Error ->
                       _conversion.value = Event.Failure(ratesResponse.message!!)
                is Resource.Success ->
                {
                    val listofresults = ratesResponse.data!!.status
                    if(ratesResponse.data!!.items != null)
                    {
                        ratesResponse.data!!.items?.let {
                            overalllist.addAll(ratesResponse.data!!.items)
                            if(listofresults == null)
                                _conversion.value = Event.Failure("Unexpected error")
                        }
                    }

              }
            }
        }
    }

    fun filter(text: String,allitem:List<Item>,selectedtab:Int)
    {
        var temp: ArrayList<Item> = ArrayList()
        for (d in allitem!!)
        {
            when(selectedtab)
            {
                1 -> if (d.title.toLowerCase().contains(text.toLowerCase()))
                    temp.add(d)
                2 -> if (d.title.toLowerCase().contains(text.toLowerCase()) && d.categories.toLowerCase().contains("java"))
                    temp.add(d)
                3 -> if (d.title.toLowerCase().contains(text.toLowerCase()) && d.categories.toLowerCase().contains("android"))
                    temp.add(d)
                4 -> if (d.title.toLowerCase().contains(text.toLowerCase()) && d.categories.toLowerCase().contains("data structure"))
                    temp.add(d)
            }
        }
        _conversion.value = Event.Filtereddata(temp)
    }

    fun blogtype(text: String,allitem:List<Item>)
    {
        var temp: ArrayList<Item> = ArrayList()
        for (d in allitem!!) {
            if (d.categories.toLowerCase().contains(text.toLowerCase()))
                temp.add(d)
        }
        _conversion.value = Event.Filtereddata(temp)
    }




}