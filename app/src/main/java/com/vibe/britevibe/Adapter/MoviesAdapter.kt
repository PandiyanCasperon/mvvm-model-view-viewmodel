package com.vibe.britevibe.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vibe.britevibe.ItemClickinterface.RecyclerViewClickListener
import com.vibe.britevibe.R
import com.vibe.britevibe.databinding.MoviesitemBinding
import com.vibe.britevibe.model.Item

class MoviesAdapter(private var movies: List<Item>, private val listener: RecyclerViewClickListener) :RecyclerView.Adapter<MoviesAdapter.MoviesHolder>()
{
    override fun getItemCount() = movies.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MoviesHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.moviesitem, parent, false))

    override fun onBindViewHolder(holder: MoviesHolder, position: Int)
    {
        holder.recyclerviewMovieBinding.movie = movies[position]
        holder.recyclerviewMovieBinding.fulllayclick.setOnClickListener {
            listener!!.onRecyclerViewItemClick(holder.recyclerviewMovieBinding.fulllayclick, movies[position])
        }
    }

    fun updateList(list: ArrayList<Item>) {
        movies = list
        notifyDataSetChanged()
    }

    inner class MoviesHolder(val recyclerviewMovieBinding: MoviesitemBinding) :
        RecyclerView.ViewHolder(recyclerviewMovieBinding.root)
}