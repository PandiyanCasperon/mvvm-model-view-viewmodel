package com.vibe.britevibe.DI


import com.vibe.britevibe.Repository.ApiProcessRepository
import com.vibe.britevibe.Repository.ApiRepository
import com.vibe.britevibe.data.MoviesApi
import com.plcoding.currencyconverter.util.DispatcherProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object AppModule
{

    @Singleton
    @Provides
    fun provideMoviesApi(): MoviesApi = Retrofit.Builder()
        .baseUrl("https://api.npoint.io/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(MoviesApi::class.java)

    @Singleton
    @Provides
    fun provideMainRepository(api: MoviesApi): ApiRepository = ApiProcessRepository(api)

    @Singleton
    @Provides
    fun provideDispatchers(): DispatcherProvider = object : DispatcherProvider {
        override val main: CoroutineDispatcher
            get() = Dispatchers.Main
        override val io: CoroutineDispatcher
            get() = Dispatchers.IO
        override val default: CoroutineDispatcher
            get() = Dispatchers.Default
        override val unconfined: CoroutineDispatcher
            get() = Dispatchers.Unconfined
    }
}