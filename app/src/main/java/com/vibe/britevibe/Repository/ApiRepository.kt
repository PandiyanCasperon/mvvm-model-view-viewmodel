package com.vibe.britevibe.Repository

import com.vibe.britevibe.model.MediumArticle
import com.plcoding.currencyconverter.util.Resource

interface ApiRepository {

    suspend fun getRecentlist(): Resource<MediumArticle>

}