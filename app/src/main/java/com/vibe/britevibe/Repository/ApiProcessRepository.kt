package com.vibe.britevibe.Repository

import com.vibe.britevibe.data.MoviesApi
import com.vibe.britevibe.model.MediumArticle
import com.plcoding.currencyconverter.util.Resource
import javax.inject.Inject

class ApiProcessRepository @Inject constructor(private val api: MoviesApi) : ApiRepository
{
    override suspend fun getRecentlist(): Resource<MediumArticle> {
        return try
        {
            val response = api.getRecentMovielist()
            val result = response.body()
            if(response.isSuccessful && result != null)
                Resource.Success(result)
            else
                Resource.Error(response.code().toString())

        } catch(e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }
}