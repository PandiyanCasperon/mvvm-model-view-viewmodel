package com.vibe.britevibe.View

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity


class SplashScreen : AppCompatActivity()
{
    //Variable Declaration
    private var timeOut:Long = 200

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity( Intent(this@SplashScreen, HomeScreen::class.java))
            finish()
        }, timeOut)
    }


}