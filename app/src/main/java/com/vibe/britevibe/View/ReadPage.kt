package com.vibe.britevibe.View

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.vibe.britevibe.BuildConfig
import com.vibe.britevibe.R
import com.vibe.britevibe.ViewModel.ReadPageViewModel
import com.vibe.britevibe.databinding.ReadpageofappBinding
import kotlinx.coroutines.flow.collect


class ReadPage : AppCompatActivity() {


    lateinit var binding: ReadpageofappBinding
    lateinit var weburl:String
    lateinit var title:String
    private val viewModel: ReadPageViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.readpageofapp)
        binding = DataBindingUtil.setContentView(this, R.layout.readpageofapp)
        weburl = intent.getStringExtra("weburl").toString()
        title = intent.getStringExtra("title").toString()
        viewModel.execute(weburl)
        lifecycleScope.launchWhenStarted {
            viewModel.conversion.collect { event ->
                when(event) {
                    is ReadPageViewModel.Event.Success -> {
                        binding!!.loader.visibility = View.GONE
                        binding.loadurl!!.loadDataWithBaseURL(weburl, event.result.toString(), "text/html", "utf-8", "")
                        binding.loadurl!!.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
                        binding.loadurl!!.webViewClient = object : WebViewClient() {
                            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                                view.loadUrl(weburl)
                                return super.shouldOverrideUrlLoading(view, request)
                            }
                        }
                    }
                    else -> Unit
                }
            }
        }

        binding.shareclick.setOnClickListener {
            shareTextUrl()
        }

        binding.backclcik.setOnClickListener {
            finish()
        }
    }

    private fun shareTextUrl() {
      Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_SUBJECT, title)
            putExtra(Intent.EXTRA_TEXT, getString(R.string.blogon)+" "+title+" "+getString(R.string.onbe)+"\n"+weburl+"\n\n"+ resources.getString(R.string.app_name)+" "+getString(R.string.downloadcontennt) + BuildConfig.APPLICATION_ID)
            startActivity(Intent.createChooser(this, getString(R.string.sharelink)))
        }

    }
}