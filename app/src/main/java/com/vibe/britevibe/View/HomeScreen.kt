package com.vibe.britevibe.View

import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.vibe.britevibe.MovieApplication
import com.vibe.britevibe.R
import com.vibe.britevibe.ViewFragment.AskqFragment
import com.vibe.britevibe.ViewFragment.FavouriteFragment
import com.vibe.britevibe.ViewFragment.HomeFragment
import com.vibe.britevibe.ViewFragment.ProfileFragment
import com.vibe.britevibe.ViewModel.HomePageViewModel
import com.vibe.britevibe.model.AuthorDetails
import dagger.hilt.android.AndroidEntryPoint
@AndroidEntryPoint
class HomeScreen : AppCompatActivity(), HomeFragment.AuthorDetailss {

    // Creates instance of the manager.
    lateinit var  appUpdateManager: AppUpdateManager
    private val viewModel: HomePageViewModel by viewModels()
    val homefragment: Fragment = HomeFragment()
    val searchfragment: Fragment = AskqFragment()
    val favourtitefragment: Fragment = FavouriteFragment()
    val settingfragment: Fragment = ProfileFragment()
    val fmd: FragmentManager = getSupportFragmentManager()
    var active = homefragment
    lateinit var fragmentRefreshListeners: FragmentRefreshListener
    var k:Int = 0
    lateinit var bottomnaviagtuonview:BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.homepage)
        appUpdateManager = AppUpdateManagerFactory.create(this)
        listener?.let { appUpdateManager.registerListener(it) }
        setUPdatabinding()
        checkUpdate()
        viewModel.execute()
    }
    private fun setUPdatabinding()
    {
        bottomnaviagtuonview = findViewById<BottomNavigationView>(R.id.bottom_menu)
        bottomnaviagtuonview.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        fragmentsetup()
    }
    fun fragmentsetup()
    {
        fmd.beginTransaction().add(R.id.fragment, settingfragment, "4").hide(settingfragment).commit()
        fmd.beginTransaction().add(R.id.fragment, favourtitefragment, "3").hide(favourtitefragment).commit()
        fmd.beginTransaction().add(R.id.fragment, searchfragment, "2").hide(searchfragment).commit()
        fmd.beginTransaction().add(R.id.fragment, homefragment, "1").commit()
    }
    private val mOnNavigationItemSelectedListener: BottomNavigationView.OnNavigationItemSelectedListener =
    object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(@NonNull item: MenuItem): Boolean {
                when (item.itemId)
                {
                    R.id.home -> {
                        fmd.beginTransaction().hide(active).show(homefragment).commit()
                        active = homefragment
                        return true
                    }
                    R.id.search -> {
                        fmd.beginTransaction().hide(active).show(searchfragment).commit()
                        active = searchfragment
                        return true
                    }
                    R.id.favourite -> {
                        fmd.beginTransaction().hide(active).show(favourtitefragment).commit()
                        active = favourtitefragment
                        return true
                    }
                    R.id.settings -> {
                        fmd.beginTransaction().hide(active).show(settingfragment).commit()
                        active = settingfragment
                        return true
                    }
                }
                return false
            }
        }
    fun setFragmentRefreshListener(fragmentRefreshListener: FragmentRefreshListener) {
        fragmentRefreshListeners = fragmentRefreshListener
    }
    interface FragmentRefreshListener {
        fun onRefresh(authordeatails: AuthorDetails)
    }
    override fun onRefresh(authordeatails: AuthorDetails) {
        fragmentRefreshListeners?.let {
            fragmentRefreshListeners.onRefresh(authordeatails)
        }
    }
    override fun onClickProfile() {
        fmd.beginTransaction().hide(active).show(settingfragment).commit()
        active = settingfragment
        bottomnaviagtuonview.selectedItemId = R.id.settings
    }
    override fun onBackPressed()
    {
        ++k
        when (k)
        {
            1 -> MovieApplication.toastmessage(getString(R.string.backmessage))
             else -> finish()
        }
    }
    private fun checkUpdate() {
        val appUpdateInfoTask = appUpdateManager?.appUpdateInfo
        appUpdateInfoTask?.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.IMMEDIATE,
                    this,
                    0)
            }
        }
    }
    private val listener: InstallStateUpdatedListener? = InstallStateUpdatedListener { installState ->
        if (installState.installStatus() == InstallStatus.DOWNLOADED) {
            MovieApplication.toastmessage(getString(R.string.downloadcomplete))
            appUpdateManager!!.completeUpdate()
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        listener?.let { appUpdateManager.unregisterListener(it) }
    }
}