package com.vibe.britevibe

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.view.View
import android.widget.Toast
import androidx.multidex.MultiDex
import com.devs.acr.AutoErrorReporter
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseApp
import com.squareup.leakcanary.LeakCanary
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber


@HiltAndroidApp
class MovieApplication : Application()
{

    init {
        instance = this
    }

    companion object {
        private var instance: MovieApplication? = null
        fun getnetworkstatus(): Boolean   {
            return isNetworkAvailable(instance!!.applicationContext)
        }

        fun isNetworkAvailable(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            var activeNetworkInfo: NetworkInfo? = null
            activeNetworkInfo = cm.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
        }

        fun snackbar(v: View,message:String)
        {
            val snackbar = Snackbar
                    .make(v, message, Snackbar.LENGTH_SHORT)
            snackbar.show()
        }
        fun toastmessage(message:String)
        {
           Toast.makeText(instance!!.applicationContext,message,Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        FirebaseApp.initializeApp(this)
        //crahreporter()
        if (BuildConfig.DEBUG) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                return
            }
            LeakCanary.install(this)
            Timber.plant(Timber.DebugTree())
        }
    }

    fun crahreporter()
    {
        AutoErrorReporter.get(this)
            .setEmailAddresses(getString(R.string.myemailid))
            .setEmailSubject(getString(R.string.crashreporter))
            .start()
    }
}
