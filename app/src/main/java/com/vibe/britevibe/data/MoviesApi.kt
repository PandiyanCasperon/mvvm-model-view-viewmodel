package com.vibe.britevibe.data

import com.vibe.britevibe.model.MediumArticle
import retrofit2.Response
import retrofit2.http.GET

interface MoviesApi
{
    @GET("70d8f1afb5aa51224b25")
    suspend fun getRecentMovielist(): Response<MediumArticle>
}