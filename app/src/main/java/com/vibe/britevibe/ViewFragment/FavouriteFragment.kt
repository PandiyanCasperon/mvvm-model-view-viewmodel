package com.vibe.britevibe.ViewFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.vibe.britevibe.R
import com.vibe.britevibe.databinding.FavouritefragmentBinding


class FavouriteFragment : Fragment()
{
    lateinit var binding:FavouritefragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View?
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.favouritefragment, container, false)
        return binding.root
    }
}