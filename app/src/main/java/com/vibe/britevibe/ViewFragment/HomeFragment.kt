package com.vibe.britevibe.ViewFragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.database.*
import com.vibe.britevibe.Adapter.MoviesAdapter
import com.vibe.britevibe.ItemClickinterface.RecyclerViewClickListener
import com.vibe.britevibe.MovieApplication
import com.vibe.britevibe.R
import com.vibe.britevibe.View.ReadPage
import com.vibe.britevibe.ViewModel.HomeScreenViewModel
import com.vibe.britevibe.databinding.HomepageofappBinding
import com.vibe.britevibe.model.AuthorDetails
import com.vibe.britevibe.model.Item
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect


@AndroidEntryPoint
class HomeFragment : Fragment(), RecyclerViewClickListener {
    var page: Int = 1
    lateinit var binding: HomepageofappBinding
    private val viewModel: HomeScreenViewModel by viewModels()
    var allitem: List<Item>? = null
    var adapter: MoviesAdapter? = null
    var selectedtab = 1
    lateinit var authorListeners: AuthorDetailss
    private lateinit var databaseReference: DatabaseReference
    var firebaseDatabase: FirebaseDatabase? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.homepageofapp, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        authorListeners = activity as AuthorDetailss
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        alldrawble()
        textchangelistener()

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = FirebaseDatabase.getInstance().reference


        binding.profiledetails.setOnClickListener {
            authorListeners?.let {
                authorListeners.onClickProfile()
            }
        }

        binding.search.setOnClickListener {
            binding.searchtext.requestFocus()
            binding.profiledetails.visibility = View.GONE
            binding.search.visibility = View.GONE
            binding.searchbox.visibility = View.VISIBLE
            showkeypad(activity!!, binding.searchtext)
        }

        binding.backclick.setOnClickListener {
            binding.searchtext.clearFocus()
            binding.searchtext.setText("")
            binding.profiledetails.visibility = View.VISIBLE
            binding.search.visibility = View.VISIBLE
            binding.searchbox.visibility = View.GONE
            hideSoftKeyboard(activity!!)
        }

        binding.close.setOnClickListener {
            binding.searchtext.setText("")
        }


        binding.alltext.setOnClickListener {
            allitem?.let { it1 ->
                adapter?.let {
                    adapter!!.updateList(it1 as ArrayList<Item>)
                }
            }
            alldrawble()
            selectedtab = 1
        }

        binding.javatext.setOnClickListener {
            filterbasedonblog(getString(R.string.java))
            javadrawble()
            selectedtab = 2
        }
        binding.kotlintext.setOnClickListener {
            filterbasedonblog(getString(R.string.android))
            kotlindrawble()
            selectedtab = 3
        }
        binding.dstext.setOnClickListener {
            filterbasedonblog(getString(R.string.ds))
            selectedtab = 4
            dsdrawble()
        }

        //Internet check
        val status = internetcheck()
        if (status)
            addFeedEventListener()
        else
        {
            MovieApplication.snackbar(binding.recylerview, getString(R.string.no_internet_connection))
            binding.loader.visibility = View.GONE
        }


        //viemodel data change listener
        lifecycleScope.launchWhenStarted {
            viewModel.conversion.collect { event ->
                when (event) {
                    is HomeScreenViewModel.Event.Success -> ondatasuccess(event.result)
                    is HomeScreenViewModel.Event.Failure -> binding.loader.visibility = View.GONE
                    is HomeScreenViewModel.Event.Loading -> binding.loader.visibility = View.VISIBLE
                    is HomeScreenViewModel.Event.Filtereddata -> adapter?.let {
                        adapter!!.updateList(event.tempdata as ArrayList<Item>)
                    }
                    else -> Unit
                }
            }
        }
    }

    //set Recylerview
    private fun setrecylerview(resultarray: List<Item>)
    {
        resultarray?.let {
            binding.recylerview.also {
                adapter = MoviesAdapter(resultarray, this)
                binding.recylerview.layoutManager = LinearLayoutManager(requireContext())
                binding.recylerview.setHasFixedSize(true)
                binding.recylerview.adapter = adapter
            }
        }
    }

    //Recylerview click listener
    override fun onRecyclerViewItemClick(view: View, movieItem: Item)
    {
        when (view.id) {
            R.id.fulllayclick -> {
                Intent(activity!!, ReadPage::class.java).apply {
                    putExtra("weburl", movieItem.link)
                    putExtra("title", movieItem.title)
                    startActivity(this)
                }

            }
        }
    }

    //All category selected
    private fun alldrawble()
    {
        binding.all.setBackground(returnselecteddrawable())
        binding.alltext.setTextColor(returntextcolorselected())

        binding.javatext.setTextColor(returntextcolorunselected())
        binding.javaborder.setBackgroundResource(0)

        binding.kotlintext.setTextColor(returntextcolorunselected())
        binding.kotlinlayout.setBackgroundResource(0)

        binding.dstext.setTextColor(returntextcolorunselected())
        binding.dslayout.setBackgroundResource(0)
    }

    //Java category selected
    private fun javadrawble()
    {
        binding.javaborder.setBackground(returnselecteddrawable())
        binding.javatext.setTextColor(returntextcolorselected())

        binding.alltext.setTextColor(returntextcolorunselected())
        binding.all.setBackgroundResource(0)

        binding.kotlintext.setTextColor(returntextcolorunselected())
        binding.kotlinlayout.setBackgroundResource(0)

        binding.dstext.setTextColor(returntextcolorunselected())
        binding.dslayout.setBackgroundResource(0)
    }

    //kotlin category selected
    private fun kotlindrawble()
    {
        binding.kotlinlayout.setBackground(returnselecteddrawable())
        binding.kotlintext.setTextColor(returntextcolorselected())

        binding.alltext.setTextColor(returntextcolorunselected())
        binding.all.setBackgroundResource(0)

        binding.javatext.setTextColor(returntextcolorunselected())
        binding.javaborder.setBackgroundResource(0)

        binding.dstext.setTextColor(returntextcolorunselected())
        binding.dslayout.setBackgroundResource(0)
    }

    //datastructure category selected
    private fun dsdrawble()
    {
        binding.dslayout.setBackground(returnselecteddrawable())
        binding.dstext.setTextColor(returntextcolorselected())

        binding.alltext.setTextColor(returntextcolorunselected())
        binding.all.setBackgroundResource(0)

        binding.javatext.setTextColor(returntextcolorunselected())
        binding.javaborder.setBackgroundResource(0)

        binding.kotlintext.setTextColor(returntextcolorunselected())
        binding.kotlinlayout.setBackgroundResource(0)
    }

    //testing internet connection
    private fun internetcheck(): Boolean
    {
        val status = MovieApplication.getnetworkstatus()
        return status
    }

    //load author details
    private fun loadprofileimage(author: AuthorDetails)
    {
        authorListeners?.let {
            authorListeners.onRefresh(author)
        }

        val options: RequestOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round)
        Glide.with(this).load(author.image).apply(options).into(binding.loadimage)

        binding.profilename.text = author.author
        binding.subcontent.text = author.role
    }

    //hide keypad
    private fun hideSoftKeyboard(activity: Activity)
    {
        val inputMethodManager: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (inputMethodManager.isAcceptingText)
            inputMethodManager.hideSoftInputFromWindow(binding.search.getWindowToken(), 0);
    }

    //show keypad
    private fun showkeypad(activity: Activity, v: View)
    {
        val inputMethodManager: InputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager!!.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT)
    }

    //search editbox listener
    private fun textchangelistener()
    {
        binding.searchtext.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                allitem?.let { viewModel.filter(s.toString(), it, selectedtab) }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isEmpty())
                    binding.close.visibility = View.INVISIBLE
                else
                    binding.close.visibility = View.VISIBLE
            }
        })
    }

    // method used for filetring data
    private fun filterbasedonblog(value: String)
    {
        allitem?.let { it1 -> viewModel.blogtype(value, it1) }
    }

    //display profile once fetched data
    private fun displayondataavailable()
    {
        binding.fullchoosesub.visibility = View.VISIBLE
        binding.fullchoose.visibility = View.VISIBLE
        binding.profilepage.visibility = View.VISIBLE
    }

    //unselected text color on category click
    private fun returntextcolorunselected(): Int
    {
        return ContextCompat.getColor(activity!!, R.color.unselectedmenucolor)
    }

    //selected text color on category click
    private fun returntextcolorselected(): Int
    {
        return ContextCompat.getColor(activity!!, R.color.purple_700)
    }

    //selected drawble on category click
    private fun returnselecteddrawable():Drawable?
    {
        return ContextCompat.getDrawable(activity!!, R.drawable.selecteddrawble)
    }

    private fun ondatasuccess(items: List<Item>)
    {
        binding.loader.visibility = View.GONE
        //load data to list on non empty case
        if (items.isNotEmpty())
        {
            setrecylerview(items)
            displayondataavailable()
            allitem =items
        }
    }

    //Interface for posting author details
    interface AuthorDetailss {
        fun onRefresh(authordeatails: AuthorDetails)
        fun onClickProfile()
    }

    //Getting Feed Data
    private fun addFeedEventListener()
    {
        databaseReference.child("feed").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val user: AuthorDetails = snapshot.getValue(AuthorDetails::class.java)!!
                //load data to author profile
                loadprofileimage(user)
                additemEventListener()
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    //Getting Blog List
    private fun additemEventListener()
    {
        databaseReference.child("items").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val items: ArrayList<Item> = ArrayList()
                for (postSnapshot in dataSnapshot.children)
                {
                    postSnapshot?.let {
                        postSnapshot.getValue(Item::class.java)?.apply {
                            items.add(this)
                        }
                    }
                }
                ondatasuccess(items)
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }
}