package com.vibe.britevibe.ViewFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.vibe.britevibe.R
import com.vibe.britevibe.databinding.AskqBinding



class AskqFragment : Fragment()
{
    lateinit var binding:AskqBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View?
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.askq, container, false)
        return binding.root
    }
}