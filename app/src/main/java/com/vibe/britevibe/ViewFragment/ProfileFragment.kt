package com.vibe.britevibe.ViewFragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.vibe.britevibe.BuildConfig
import com.vibe.britevibe.R
import com.vibe.britevibe.View.HomeScreen
import com.vibe.britevibe.databinding.ProfilesfragmentBinding
import com.vibe.britevibe.model.AuthorDetails


class ProfileFragment : Fragment()
{
    lateinit var binding:ProfilesfragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DataBindingUtil.inflate(inflater, R.layout.profilesfragment, container, false)
        (activity as HomeScreen?)!!.setFragmentRefreshListener(object : HomeScreen.FragmentRefreshListener {
            override fun onRefresh(authordeatails: AuthorDetails) {
                updateprofiledetails(authordeatails)
            }
        })

        binding.mediumlink.setOnClickListener {
           Intent(Intent.ACTION_VIEW, Uri.parse(binding.mediumlink.text.toString())).apply {
               startActivity(this)
           }
        }

        binding.share.setOnClickListener {
           Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT,
                        resources.getString(R.string.app_name)+" "+getString(R.string.downloadcontennt) + BuildConfig.APPLICATION_ID)
                type = "text/plain"
                startActivity(this)
            }
        }
        return binding.root
    }

    fun updateprofiledetails(author: AuthorDetails)
    {
        val options: RequestOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round)
        Glide.with(this).load(author.image).apply(options).into(binding.loadimage)

        binding.profilename.text = author.author
        binding.role.text = author.role
        binding.story.text = author.story

        binding.adnroidcount.text = author.androidcount
        binding.dscount.text = author.datastructurecount
        binding.javacount.text = author.javacount

        val content = SpannableString(author.description)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        binding.mediumlink.setText(content)
    }


}