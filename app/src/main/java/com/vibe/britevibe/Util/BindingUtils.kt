package com.vibe.britevibe.Util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.vibe.britevibe.R

@BindingAdapter("profileimage")
fun loadImage(image: ImageView, url: String)
{
    val options: RequestOptions = RequestOptions()
            .centerCrop()
            .placeholder(R.mipmap.ic_launcher_round)
            .error(R.mipmap.ic_launcher_round)
    Glide.with(image).load(url).apply(options).into(image)
}