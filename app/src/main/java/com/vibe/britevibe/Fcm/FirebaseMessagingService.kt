package com.vibe.britevibe.Fcm

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.vibe.britevibe.R
import com.vibe.britevibe.View.SplashScreen
import org.json.JSONObject


class FirebaseMessagingService : FirebaseMessagingService() {

    var min = 50
    var max = 100
    private var random_int = 1
    lateinit var notificationChannel: NotificationChannel
    lateinit var notificationManager: NotificationManager
    lateinit var builder: Notification.Builder

    @SuppressLint("LongLogTag")
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val data = remoteMessage.notification
        data?.let {
           random_int = Math.floor(Math.random() * (max - min + 1) + min).toInt()
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            showNotification(data.title, data.body)
        }
    }

    private fun showNotification(title: String?, body: String?)
    {
        val intent = Intent(this, SplashScreen::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            notificationChannel = NotificationChannel(
                "1",
                body,
                NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(notificationChannel)
            builder = Notification.Builder(this, "1").setContentTitle(title).setContentText(body).setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent).setAutoCancel(true)
        }
        notificationManager.notify(random_int, builder.build())
    }
}